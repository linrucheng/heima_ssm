package com.itheima.ssm.test;

import com.itheima.ssm.domain.Orders;
import com.itheima.ssm.service.IOrdresService;
import com.itheima.ssm.service.IProductService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @program: heima_ssm
 * @name: ServiceTest
 * @author;lin
 * @create: 2019-06-19 01:29
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext.xml")
public class ServiceTest {
   @Autowired
    private IOrdresService os;
   @Test
   public void testFindByOid() throws Exception{
       //测试根据id查询所有信息
       Orders orders = os.findByOid("0E7231DC797C486290E8713CA3C6ECCC") ;
       System.out.println(orders);
       //Orders{id='0E7231DC797C486290E8713CA3C6ECCC', orderNum='12345', orderTime=Fri Mar 02 12:00:00 CST 2018,
       // orderTimeStr='null', orderStatus=1, orderStatusStr='null', peopleCount=2, product=Product{id='676C5BD1D35E429A8C2E114939C5685A',
       // productNum='itcast-002', productName='北京三日游', cityName='北京', departureTime=Wed Oct 10 10:10:00 CST 2018, departureTimeStr='null',
       // productPrice=1200.0, productDesc='不错的旅行', productStatus=1, productStatusStr='null'},
       // travellers=[Traveller{id='3FE27DF2A4E44A6DBC5D0FE4651D3D3E', name='张龙', sex='男', phoneNum='13333333333', credentialsType=0, credentialsTypeStr='null', credentialsNum='123456789009876543', travellerType=0, travellerTypeStr='null'}],
       // member=Member{id='E61D65F673D54F68B0861025C69773DB', name='张三', nickname='小三', phoneNum='18888888888', email='zs@163.com'}, payType=0, payTypeStr='null', orderDesc='没什么'}

   }

}




