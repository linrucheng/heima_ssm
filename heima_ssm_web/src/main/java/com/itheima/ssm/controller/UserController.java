package com.itheima.ssm.controller;

import com.itheima.ssm.domain.Role;
import com.itheima.ssm.domain.UserInfo;
import com.itheima.ssm.service.IRoleService;
import com.itheima.ssm.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @program: heima_ssm
 * @name: UserController
 * @author;lin
 * @create: 2019-06-21 08:34
 * 用户的控制器
 **/
@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private IUserService userService;
    @Autowired
    private IRoleService roleService;

    @RequestMapping("/findAll.do")
    public ModelAndView findAll() throws Exception {
        //查询所有
        List<UserInfo> userList = userService.findAll();
        ModelAndView mv = new ModelAndView();
        mv.addObject("userList", userList);
        mv.setViewName("user-list");
        return mv;
    }

    @RequestMapping("/save.do")
    public String saveUser(UserInfo userInfo) throws Exception {
        //保存用户
        userService.saveUser(userInfo);
        return "redirect:findAll.do";
    }

    @RequestMapping("/findById.do")
    public ModelAndView findUserById(@RequestParam(name = "id", required = true) String id) throws Exception {
        //根据用户id查询
        UserInfo userInfo = userService.findById(id);
        ModelAndView mv = new ModelAndView();
        mv.addObject("user", userInfo);
        mv.setViewName("user-show");
        return mv;
    }

    @RequestMapping("/findUserByIdAndOtherAllRole.do")
    public ModelAndView findUserByIdAndOtherAllRole(String id) throws Exception {
        //查询用户和其没有的角色信息
        ModelAndView mv = new ModelAndView();

        List<Role> list = roleService.findOtherRoles(id);//查询用户其他的没有添加的角色角色
        mv.addObject("id", id);
        mv.addObject("roleList", list);
        mv.setViewName("user-role-add");
        return mv;
    }

    @RequestMapping("/addRoleToUser.do")
    public String addRoleToUser(String userId, String[] ids) throws Exception {
        //给用户关联角色
        userService.addRoleToUser(userId, ids);
        return "redirect:findUserByIdAndOtherAllRole.do?id=" + userId;
    }

    //findByUid.do
    @RequestMapping("/findByUid.do")
    public ModelAndView findByUid(String id) throws Exception {
        //根据id查询用户信息
        UserInfo userInfo = userService.findById(id);
        ModelAndView mv = new ModelAndView();
        mv.addObject("user", userInfo);
        mv.setViewName("user-update");
        return mv;
    }

    @RequestMapping("/update.do")
    public String update(UserInfo userInfo) throws Exception {
        //修改用户信息
        userService.update(userInfo);
        return "redirect:findAll.do";
    }

    @RequestMapping("/delete.do")
    public String delete(String id) throws Exception {
        //删除用户
        userService.delete(id);
        return "redirect:findAll.do";
    }

    @RequestMapping("/findUserByIdAndAllRole.do")
    public ModelAndView findUserByIdAndAllRole(String id) throws Exception {
        //查询用户的所有角色信息
        UserInfo userInfo = userService.findById(id);
        List<Role> roles = userInfo.getRoles();
        ModelAndView mv = new ModelAndView();
        mv.addObject("roleList", roles);
        mv.addObject("id", id);
        mv.setViewName("user-role-delete");
        return mv;
    }

    //删除用户所拥有的角色 deleteRoleToUser,即删除中间表
    @RequestMapping("/deleteRoleToUser.do")
    public String deleteRoleToUser(String userId, String[] ids) throws Exception {
        userService.deleteUserAndAllRoles(userId, ids);
        //跳转到用户拥有的角色页面
        return "redirect:findUserByIdAndAllRole.do?id=" + userId;
    }


}




