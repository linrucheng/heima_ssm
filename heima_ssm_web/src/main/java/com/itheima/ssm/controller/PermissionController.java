package com.itheima.ssm.controller;

import com.itheima.ssm.domain.Permission;
import com.itheima.ssm.service.IPermssionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @program: heima_ssm
 * @name: PermissionController
 * @author;lin
 * @create: 2019-06-22 00:27
 * 权限的控制器
 **/
@Controller
@RequestMapping("/permission")
public class PermissionController {
    @Autowired
    private IPermssionService permssionService;

    @RequestMapping("/findAll.do")
    public ModelAndView findAll() throws Exception {
        //查询所有权限
        List<Permission> list = permssionService.findAll();
        ModelAndView mv = new ModelAndView();
        mv.addObject("permissionList", list);
        mv.setViewName("permission-list");
        return mv;
    }

    @RequestMapping("/save.do")
    public String save(Permission permission) throws Exception {
        //保存权限
        permssionService.save(permission);
        return "redirect:/permission/findAll.do";
    }


}




