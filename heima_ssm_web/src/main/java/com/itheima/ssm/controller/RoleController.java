package com.itheima.ssm.controller;

import com.itheima.ssm.domain.Permission;
import com.itheima.ssm.domain.Role;
import com.itheima.ssm.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @program: heima_ssm
 * @name: RoleController
 * @author;lin
 * @create: 2019-06-21 23:54
 * 角色的控制器
 **/
@Controller
@RequestMapping("/role")
public class RoleController {
    @Autowired
    private IRoleService roleService;


    @RequestMapping("/findAll.do")
    public ModelAndView findAll() throws Exception {
        //查询所有的角色
        ModelAndView mv = new ModelAndView();
        List<Role> list = roleService.findAll();
        mv.addObject("roleList", list);
        mv.setViewName("role-list");
        return mv;
    }

    @RequestMapping("/save.do")
    public String save(Role role) throws Exception {
        //角色保存
        roleService.save(role);
        return "redirect:/role/findAll.do";
    }

    //查询角色所没有的权限findRoleByIdAndAllPermission.do
    @RequestMapping("/findRoleByIdAndAllPermission.do")
    public ModelAndView findRoleByIdAndAllPermission(String id) throws Exception {
        //查询角色没有的权限
        ModelAndView mv = new ModelAndView();
        List<Permission> permissionList = roleService.findOtherPermission(id);
        mv.addObject("permissionList", permissionList);
        mv.addObject("id", id);
        mv.setViewName("role-permisssion-add");
        return mv;
    }

    //给角色添加没有的权限addPermissionToRole
    @RequestMapping("/addPermissionToRole.do")
    public String addPermissionToRole(String roleId, String[] ids) throws Exception {
        //给角色添加没有的权限
        roleService.addPermissionToRole(roleId, ids);
        return "redirect:findAll.do";
    }
}







