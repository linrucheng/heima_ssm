package com.itheima.ssm.controller;

import com.github.pagehelper.PageInfo;
import com.itheima.ssm.domain.SysLog;
import com.itheima.ssm.service.ISysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @program: heima_ssm
 * @name: SysLogController
 * @author;lin
 * @create: 2019-06-23 18:32
 **/
@Controller
@RequestMapping("/sysLog")
public class SysLogController {
    @Autowired
    private ISysLogService sysLogService;

    @RequestMapping("/findAll.do")
    public ModelAndView findAll(@RequestParam(name = "currentPage", required = true, defaultValue = "1") int currentPage,
                                @RequestParam(name = "pageSize", required = true, defaultValue = "4") int pageSize) throws Exception {
        //查询所有的日志信息
        List<SysLog> logList = sysLogService.findAll(currentPage, pageSize);
        PageInfo pageInfo = new PageInfo(logList);
        ModelAndView mv = new ModelAndView();
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("sysLog-list");
        return mv;
    }

    //分页
    public ModelAndView findByPage() throws Exception {
        return null;
    }


}





