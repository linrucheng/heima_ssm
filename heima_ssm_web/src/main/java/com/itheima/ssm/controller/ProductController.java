package com.itheima.ssm.controller;

import com.github.pagehelper.PageInfo;
import com.itheima.ssm.domain.Product;
import com.itheima.ssm.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.security.RolesAllowed;
import java.util.List;

/**
 * @program: heima_ssm
 * @name: ProductController
 * @author;lin
 * @create: 2019-06-14 21:03
 **/
@Controller
@RequestMapping("/product")
@SuppressWarnings("unchecked")
public class ProductController {
    @Autowired
    private IProductService ps;

/*    @RequestMapping("/findAll.do")
    public ModelAndView findAllProduct() throws Exception {
        //查询所有的product
        ModelAndView mv = new ModelAndView();
        List<Product> products = ps.findAllProduct();
        mv.addObject("productList", products);
        mv.setViewName("product-list");
        return mv;
    }*/


    //product增加分页
    @RequestMapping("/findAll.do")

    public ModelAndView findByPage(@RequestParam(name = "currentPage", required = true, defaultValue = "1") int page,
                                   @RequestParam(name = "pageSize", required = true, defaultValue = "4") int size) throws Exception {
        //
        List<Product> products = ps.findAllProduct(page, size);
        PageInfo pageInfo = new PageInfo(products);
        ModelAndView mv = new ModelAndView();
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("product-list");
        return mv;
    }


    //添加
    @RequestMapping("/save.do")
    public String saveProduct(Product product) {
        ps.saveProduct(product);
        return "redirect:/product/findAll.do";
    }

}










