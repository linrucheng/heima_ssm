package com.itheima.ssm.controller;

import com.itheima.ssm.domain.SysLog;
import com.itheima.ssm.service.ISysLogService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HttpServletBean;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Date;


/**
 * @program: heima_ssm
 * @name: LogAop
 * @author;lin
 * @create: 2019-06-23 12:30
 * 切面的类
 **/
@Component
@Aspect
@Scope("prototype")
public class LogAop {

    private Date startTime;//访问时间
    private Class clazz;//访问的类
    private Method method;//访问的方法

    @Autowired
    private HttpServletRequest request;
    @Autowired
    private ISysLogService sysLogService;


    /*配置切入点表达式 只有增删改和登录被记录 不包含*/
    @Pointcut("execution(* com.itheima.ssm.service.impl.UserServiceImpl.loadUserByUsername(..))||execution(* com.itheima.ssm.controller.*.*(..))&&!execution(* com.itheima.ssm.controller.*.find*(..))")
    private void pt1() {
    }
    //execution(* com...*Service+.*(..)) && !execution(* Object.*(..))

    @Before("pt1()")
    public void doBefore(JoinPoint joinPoint) {
        startTime = new Date();//获取访问时间
        clazz = joinPoint.getTarget().getClass();//访问的类
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        method = methodSignature.getMethod();//访问的方法
    }

    @After("pt1()")
    public void doAfter(JoinPoint joinPoint) {
        long time = new Date().getTime() - startTime.getTime();//操作时长
        String uri = request.getRequestURI();//获取url
        String ip = request.getRemoteAddr();//获取ip地址
        String name = SecurityContextHolder.getContext().getAuthentication().getName();//获取登录人的姓名
        SysLog sysLog = new SysLog();
        sysLog.setExecutionTime(time);//设置操作时长
        sysLog.setIp(ip);//设置ip
        sysLog.setUrl(uri);//设置请求参数的路径
        sysLog.setUsername(name);//设置登录人姓名
        sysLog.setVisitTime(startTime);//设置登录时间
        String methodName = clazz.getName().substring(clazz.getName().lastIndexOf(".") + 1);
        sysLog.setMethod("[类名]" + methodName + "[方法名]" + method.getName());
        sysLogService.save(sysLog);
    }


}




