package com.itheima.ssm.controller;

import com.github.pagehelper.PageInfo;
import com.itheima.ssm.domain.Orders;
import com.itheima.ssm.service.IOrdresService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.security.RolesAllowed;
import java.util.List;

/**
 * @program: heima_ssm
 * @name: OrderController
 * @author;lin
 * @create: 2019-06-17 18:19
 **/
@Controller
@RequestMapping("/orders")
public class OrdersController {
    @Autowired
    private IOrdresService ordresService;

    //订单页面展示+分页
    @RequestMapping("/findAll.do")
    public ModelAndView findAllOrder(@RequestParam(name = "currentPage", required = true, defaultValue = "1") Integer Page,
                                     @RequestParam(name = "pageSize", required = true, defaultValue = "4") Integer Size) throws Exception {
        ModelAndView mv = new ModelAndView();
        System.out.println(Page);
        System.out.println(Size);
        List<Orders> orders = ordresService.findAllByPage(Page, Size);
        //在java代码中获取登录人的姓名
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        System.out.println(name);

        PageInfo pageInfo = new PageInfo(orders);//分页bean
        mv.addObject("pageInfo", pageInfo);
        //跳转页面
        mv.setViewName("orders-list");
        return mv;
    }

    //根据id查询所有信息
    @RequestMapping("/findById.do")
    public ModelAndView findById(String id) throws Exception {
        ModelAndView mv = new ModelAndView();
        Orders orders = ordresService.findByOid(id);
        mv.addObject("orders", orders);
        mv.setViewName("order-show");
        return mv;
    }
}







