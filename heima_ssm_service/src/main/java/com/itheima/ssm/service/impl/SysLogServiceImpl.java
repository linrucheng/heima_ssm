package com.itheima.ssm.service.impl;

import com.github.pagehelper.PageHelper;
import com.itheima.ssm.dao.ISysLogDao;
import com.itheima.ssm.domain.SysLog;
import com.itheima.ssm.service.ISysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @program: heima_ssm
 * @name: SysLogServiceImpl
 * @author;lin
 * @create: 2019-06-23 16:47
 **/
@Service
@Transactional
public class SysLogServiceImpl implements ISysLogService {

    @Autowired
    private ISysLogDao sysLogDao;

    @Override
    public void save(SysLog sysLog) {
        //保存日志
        sysLogDao.save(sysLog);

    }

    @Override
    public List<SysLog> findAll(int currentPage, int pageSize) throws Exception {
        //分页
        PageHelper.startPage(currentPage, pageSize);
        return sysLogDao.findAll();
    }


}




