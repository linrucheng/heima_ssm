package com.itheima.ssm.service;

import com.itheima.ssm.domain.Permission;
import com.itheima.ssm.domain.Role;

import java.util.List;

public interface IRoleService {


    List<Role> findAll()throws Exception;

    void save(Role role) throws Exception;

    List<Role> findOtherRoles(String userId) throws Exception;


    List<Permission> findOtherPermission(String id) throws Exception;

    void addPermissionToRole(String roleId, String[] ids) throws Exception;
}
