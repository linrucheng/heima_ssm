package com.itheima.ssm.service.impl;

import com.itheima.ssm.dao.IPermissionDao;
import com.itheima.ssm.domain.Permission;
import com.itheima.ssm.service.IPermssionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @program: heima_ssm
 * @name: PermssionService
 * @author;lin
 * @create: 2019-06-22 00:32
 **/
@Service
@Transactional
public class PermssionService implements IPermssionService {
    @Autowired
    private IPermissionDao permissionDao;

    @Override
    public List<Permission> findAll() throws Exception {
        List<Permission> list = permissionDao.findAll();
        return list;
    }

    @Override
    public void save(Permission permission) throws Exception {
         permissionDao.save(permission);
    }
}




