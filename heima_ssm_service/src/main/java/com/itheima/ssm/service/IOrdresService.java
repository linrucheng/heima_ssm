package com.itheima.ssm.service;

import com.itheima.ssm.domain.Orders;

import java.util.List;

public interface IOrdresService {
    List<Orders> findAll() throws Exception;

    List<Orders> findAllByPage(Integer currentPage, Integer pageSize) throws Exception;

    Orders findByOid(String id) throws Exception;
}
