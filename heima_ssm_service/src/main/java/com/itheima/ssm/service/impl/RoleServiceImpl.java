package com.itheima.ssm.service.impl;

import com.itheima.ssm.dao.IPermissionDao;
import com.itheima.ssm.dao.IRoleDao;
import com.itheima.ssm.domain.Permission;
import com.itheima.ssm.domain.Role;
import com.itheima.ssm.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @program: heima_ssm
 * @name: RoleServiceImpl
 * @author;lin
 * @create: 2019-06-21 23:58
 **/
@Service
@Transactional
public class RoleServiceImpl implements IRoleService {
    @Autowired
    private IRoleDao roleDao;
    @Autowired
    private IPermissionDao permissionDao;

    //查询所有角色
    @Override
    public List<Role> findAll() throws Exception {
        List<Role> list = roleDao.findAll();

        return list;
    }

    @Override
    public void save(Role role) throws Exception {
        roleDao.save(role);

    }

    @Override
    public List<Role> findOtherRoles(String userId) throws Exception {
        //调用dao层查询其他的角色
        List<Role> list = roleDao.findOtherRole(userId);
        return list;
    }

    @Override
    public List<Permission> findOtherPermission(String roleId) throws Exception {
        //查询角色没有的权限
        List<Permission> list = permissionDao.findOtherPermission(roleId);
        return list;
    }

    @Override
    public void addPermissionToRole(String roleId, String[] ids) throws Exception {
        //给角色添加没有的权限
        for (String id : ids) {
            permissionDao.addPermissionToRole(roleId, id);
        }
    }


}




