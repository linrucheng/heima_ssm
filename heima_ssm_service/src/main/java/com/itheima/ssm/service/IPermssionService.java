package com.itheima.ssm.service;

import com.itheima.ssm.domain.Permission;

import java.util.List;

public interface IPermssionService {

    List<Permission> findAll() throws Exception;

    void save(Permission permission) throws Exception;
}
