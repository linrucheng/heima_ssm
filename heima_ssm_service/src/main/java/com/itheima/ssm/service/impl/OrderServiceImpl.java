package com.itheima.ssm.service.impl;

import com.github.pagehelper.PageHelper;
import com.itheima.ssm.dao.IOrdersDao;
import com.itheima.ssm.domain.Orders;
import com.itheima.ssm.service.IOrdresService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @program: heima_ssm
 * @name: OrderServiceImpl
 * @author;lin
 * @create: 2019-06-18 08:53
 **/
@Service
@Transactional
public class OrderServiceImpl implements IOrdresService {
    @Autowired
    private IOrdersDao ordersDao;

    @Override
    public List<Orders> findAll() throws Exception {
        //查询所有
        List<Orders> orders = ordersDao.findAll();
        return orders;
    }

    @Override
    public List<Orders> findAllByPage(Integer currentPage, Integer pageSize) throws Exception {
        //分页
        PageHelper.startPage(currentPage, pageSize);

        List<Orders> orders = ordersDao.findAll();
        return orders;
    }

    @Override
    public Orders findByOid(String id) throws Exception {
        //根据id查询所有信息
        Orders orders = ordersDao.findByOid(id);

        return orders;
    }


}




