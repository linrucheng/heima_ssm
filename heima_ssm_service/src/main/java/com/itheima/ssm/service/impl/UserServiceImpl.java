package com.itheima.ssm.service.impl;

import com.itheima.ssm.dao.IUserDao;
import com.itheima.ssm.domain.Role;
import com.itheima.ssm.domain.UserInfo;
import com.itheima.ssm.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: heima_ssm
 * @name: UserServiceImpl
 * @author;lin
 * @create: 2019-06-20 19:05
 **/
@Service
@Transactional
public class UserServiceImpl implements IUserService, UserDetailsService {
    @Autowired
    private IUserDao userDao;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserInfo userInfo = null;
        try {
            userInfo = userDao.loginUser(s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*Collection<? extends GrantedAuthority> authorities*/
        User user = new User(userInfo.getUsername(), userInfo.getPassword(),
                userInfo.getStatus() == 0 ? false : true, true, true, true, getAuthorities(userInfo.getRoles()));
        return user;
    }


    //获取用户角色
    public List<SimpleGrantedAuthority> getAuthorities(List<Role> roles) {
        //创建自定义权限
        ArrayList<SimpleGrantedAuthority> list = new ArrayList<SimpleGrantedAuthority>();
        //遍历查询到的角色
        for (Role role : roles) {
            list.add(new SimpleGrantedAuthority("ROLE_" + role.getRoleName()));
        }
        return list;
    }


    @Override
    public List<UserInfo> findAll() throws Exception {
        //查询所有用户
        List<UserInfo> userList = userDao.findAll();
        return userList;
    }

    @Override
    public void saveUser(UserInfo userInfo) throws Exception {
        //对密码进行加密
        userInfo.setPassword(bCryptPasswordEncoder.encode(userInfo.getPassword()));
        //调用dao层保存用户 ,
        userDao.saveUser(userInfo);
    }


    @Override
    public UserInfo findById(String id) throws Exception {
        //根据id查询用户
        UserInfo userInfo = userDao.findById(id);
        return userInfo;
    }

    @Override
    public void addRoleToUser(String userId, String[] roleIds) throws Exception {
        //给用户关联角色
        for (String roleId : roleIds) {
            userDao.addRoleToUser(userId, roleId);
        }
    }

    @Override
    public void update(UserInfo userInfo) throws Exception {
        //修改用户新信息
        userDao.update(userInfo);
    }

    @Override
    public void delete(String useId) throws Exception {
        //删除用户,先删除中间表记录,再删除user表记录
        userDao.deleteUserAndRole(useId);
        userDao.deleteUser(useId);
    }

    @Override
    public void deleteUserAndAllRoles(String userId, String[] ids) throws Exception {
        //删除用户的角色信息,即删除中间表记录
        for (String id : ids) {
            userDao.deleteByUidAndRoleId(userId,id);
        }
    }



}






