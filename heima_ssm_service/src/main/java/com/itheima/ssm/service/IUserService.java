package com.itheima.ssm.service;

import com.itheima.ssm.domain.UserInfo;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface IUserService {


    List<UserInfo> findAll() throws Exception;

    void saveUser(UserInfo userInfo) throws Exception;

    UserInfo findById(String id) throws Exception;

    void addRoleToUser(String userId, String[] ids) throws Exception;

    void update(UserInfo userInfo) throws Exception;

    void delete(String id)throws Exception;


    void deleteUserAndAllRoles(String userId, String[] ids) throws Exception;
}
