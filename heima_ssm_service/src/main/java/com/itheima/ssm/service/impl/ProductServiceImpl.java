package com.itheima.ssm.service.impl;

import com.github.pagehelper.PageHelper;
import com.itheima.ssm.dao.IProductDao;
import com.itheima.ssm.domain.Product;
import com.itheima.ssm.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @program: heima_ssm
 * @name: ProductServiceImpl
 * @author;lin
 * @create: 2019-06-14 20:20
 * 查询所有
 **/
@Service
@Transactional
public class ProductServiceImpl implements IProductService {
    @Autowired
    private IProductDao productDao;

    @Override
    public List<Product> findAllProduct(int page, int size) throws Exception {
        //查询所有的products 增加分页
        PageHelper.startPage(page, size);


        List<Product> products = productDao.findAllProduct();
        return products;
    }

    @Override
    public void saveProduct(Product product) {
        //保存product
        productDao.saveProduct(product);
    }
}




