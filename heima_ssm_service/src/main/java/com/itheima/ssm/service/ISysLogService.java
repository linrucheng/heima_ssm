package com.itheima.ssm.service;

import com.itheima.ssm.domain.SysLog;

import java.util.List;

public interface ISysLogService {

    public void save(SysLog sysLog);

    List<SysLog> findAll(int currentPage,int pageSize) throws Exception;
}
