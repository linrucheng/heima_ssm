package com.itheima.ssm.domain;

import java.io.Serializable;
import java.util.List;

/**
 * @program: heima_ssm
 * @name: Permission
 * @author;lin
 * @create: 2019-06-19 20:19
 * 权限的实体类
 **/
public class Permission implements Serializable {
    private String id;//主键id
    private String permissionName;//权限名
    private String url;//url
    private List<Role> roles;//角色名

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}




