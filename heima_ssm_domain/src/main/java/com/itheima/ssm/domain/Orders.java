package com.itheima.ssm.domain;

import com.itheima.ssm.util.DateUtil;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @program: heima_ssm
 * @name: Orders
 * @author;lin
 * @create: 2019-06-17 17:01
 * 订单的实体类
 **/
public class Orders implements Serializable {
    private String id; //id主键uuid
    private String orderNum;//订单编号
    private Date orderTime;//下单时间
    private String orderTimeStr;//下单时间字符串类型
    private int orderStatus;//订单状态 订单状态(0 未支付 1 已支付)
    private String orderStatusStr;//字符串订单状态
    private int peopleCount;//出行人数
    private Product product;//产品 产品和订单是一对多
    private List<Traveller> travellers;//
    private Member member;//联系人 一对一查询
    private Integer payType;//支付方式(0 支付宝 1 微信 2其它)
    private String payTypeStr;//支付方式(0 支付宝 1 微信 2其它)
    private String orderDesc;//订单描述
    private Integer travellersSize;//出游人数

    public Integer getTravellersSize() {
        //出游人数
        if (travellers!=null){
            travellersSize=travellers.size();
        }

        return travellersSize;
    }

    public void setTravellersSize(Integer travellersSize) {
        this.travellersSize = travellersSize;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public String getOrderTimeStr() {
        //下单时间字符串
        if (orderTime != null) {
            orderTimeStr = DateUtil.date2String(orderTime, "yyyy-MM-dd HH:mm:ss");
        }
        return orderTimeStr;
    }


    public void setOrderTimeStr(String orderTimeStr) {
        this.orderTimeStr = orderTimeStr;
    }

    public int getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(int orderStatus) {
        this.orderStatus = orderStatus;
    }

    public int getPeopleCount() {
        return peopleCount;
    }

    public void setPeopleCount(int peopleCount) {
        this.peopleCount = peopleCount;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public List<Traveller> getTravellers() {
        return travellers;
    }

    public void setTravellers(List<Traveller> travellers) {
        this.travellers = travellers;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public String getPayTypeStr() {
        //支付方式
        if (payType == 0) {
            payTypeStr = "支付宝";
            return payTypeStr;
        }
        if (payType == 1) {
            payTypeStr = "微信";
            return payTypeStr;
        }
        payTypeStr = "其他";

        return payTypeStr;
    }

    public void setPayTypeStr(String payTypeStr) {
        this.payTypeStr = payTypeStr;
    }

    public String getOrderDesc() {
        return orderDesc;
    }

    public void setOrderDesc(String orderDesc) {
        this.orderDesc = orderDesc;
    }

    public String getOrderStatusStr() {
        //订单状态订单状态(0 未支付 1 已支付)
        if (orderStatus == 0) {
            orderStatusStr = "未支付";
        }
        if (orderStatus == 1) {
            orderStatusStr = "已支付";
        }


        return orderStatusStr;
    }

    public void setOrderStatusStr(String orderStatusStr) {
        this.orderStatusStr = orderStatusStr;
    }

    @Override
    public String toString() {
        return "Orders{" +
                "id='" + id + '\'' +
                ", orderNum='" + orderNum + '\'' +
                ", orderTime=" + orderTime +
                ", orderTimeStr='" + orderTimeStr + '\'' +
                ", orderStatus=" + orderStatus +
                ", orderStatusStr='" + orderStatusStr + '\'' +
                ", peopleCount=" + peopleCount +
                ", product=" + product +
                ", travellers=" + travellers +
                ", member=" + member +
                ", payType=" + payType +
                ", payTypeStr='" + payTypeStr + '\'' +
                ", orderDesc='" + orderDesc + '\'' +
                '}';
    }
}




