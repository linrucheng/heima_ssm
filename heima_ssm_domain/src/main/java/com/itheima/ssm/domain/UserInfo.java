package com.itheima.ssm.domain;

import java.io.Serializable;
import java.util.List;

/**
 * @program: heima_ssm
 * @name: UserInfo
 * @author;lin
 * @create: 2019-06-19 20:10
 **/
public class UserInfo implements Serializable {
    //用户的实体类
    private String id;//id
    private String username;//用户名
    private String email;//email
    private String password;//密码(加密)
    private String phoneNum;//电话号码
    private Integer status;//状态 0未开启 1开启
    private String statusStr;//字符串类型
    private List<Role> roles;//角色 多对多

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatusStr() {
        //用户的状态 0未开启 1开启
        if (status == 0) {
            statusStr = "未开启";
        }
        if (status == 1) {
            statusStr = "开启";
        }


        return statusStr;
    }

    public void setStatusStr(String statusStr) {
        this.statusStr = statusStr;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}





