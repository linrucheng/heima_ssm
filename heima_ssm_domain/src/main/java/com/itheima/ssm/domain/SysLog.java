package com.itheima.ssm.domain;

import com.itheima.ssm.util.DateUtil;

import java.io.Serializable;
import java.util.Date;

/**
 * @program: heima_ssm
 * @name: SysLog
 * @author;lin
 * @create: 2019-06-23 08:
 * 日志的实体类
 **/
public class SysLog implements Serializable {
    private String id;
    private Date visitTime;//访问时间
    private String visitTimeStr;//字符串类型
    private String username;//访问者姓名
    private String ip;//访问者ip
    private String url;//访问者url
    private Long executionTime;//执行时长
    private String method;//访问方法

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getVisitTime() {
        return visitTime;
    }

    public void setVisitTime(Date visitTime) {
        this.visitTime = visitTime;
    }

    public String getVisitTimeStr() {
        visitTimeStr = DateUtil.date2String(visitTime, "yyyy-MM-dd HH:mm:ss");
        return visitTimeStr;
    }

    public void setVisitTimeStr(String visitTimeStr) {
        this.visitTimeStr = visitTimeStr;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(Long executionTime) {
        this.executionTime = executionTime;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }
}




