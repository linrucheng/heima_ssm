package com.itheima.ssm.domain;

import java.io.Serializable;
import java.util.List;

/**
 * @program: heima_ssm
 * @name: Role
 * @author;lin
 * @create: 2019-06-19 20:12
 * 角色的实体类
 **/
public class Role implements Serializable {
    private String id;//id
    private String roleName;//角色名
    private String roleDesc;//角色描述
    private List<Permission> permissions;//权限
    private List<UserInfo> userInfos;//用户,与user是多对多关系

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleDesc() {
        return roleDesc;
    }

    public void setRoleDesc(String roleDesc) {
        this.roleDesc = roleDesc;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }

    public List<UserInfo> getUserInfos() {
        return userInfos;
    }

    public void setUserInfos(List<UserInfo> userInfos) {
        this.userInfos = userInfos;
    }
}




