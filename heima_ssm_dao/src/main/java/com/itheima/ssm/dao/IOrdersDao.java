package com.itheima.ssm.dao;

import com.itheima.ssm.domain.Orders;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IOrdersDao {

    //订单查询所有订单 产品和订单是一对多关系
    @Select("select*from orders")
    @Results({
            @Result(id = true, column = "id", property = "id"),
            @Result(column = "ORDERNUM", property = "orderNum"),
            @Result(column = "ORDERTIME", property = "orderTime"),
            @Result(column = "PEOPLECOUNT", property = "peopleCount"),
            @Result(column = "ORDERDESC", property = "orderDesc"),
            @Result(column = "PAYTYPE", property = "payType"),
            @Result(column = "ORDERSTATUS", property = "orderStatus"),
            @Result(column = "PRODUCTID", property = "product",
                    one = @One(select = "com.itheima.ssm.dao.IProductDao.findByPid"))
    })
    List<Orders> findAll() throws Exception;






   //根据order的id查询
    @Select("select *from orders where id=#{id}")
    @Results({
            @Result(id = true, column = "id", property = "id"),
            @Result(column = "ORDERNUM", property = "orderNum"),
            @Result(column = "ORDERTIME", property = "orderTime"),
            @Result(column = "PEOPLECOUNT", property = "peopleCount"),
            @Result(column = "ORDERDESC", property = "orderDesc"),
            @Result(column = "PAYTYPE", property = "payType"),
            @Result(column = "ORDERSTATUS", property = "orderStatus"),
            @Result(column = "PRODUCTID", property = "product",
                    one = @One(select = "com.itheima.ssm.dao.IProductDao.findByPid")),
            @Result(column = "MEMBERID", property = "member",
                    one = @One(select = "com.itheima.ssm.dao.IMemberDao.findByMid")),
            @Result(column = "id",property ="travellers",javaType =java.util.List.class,
                    many = @Many(select = "com.itheima.ssm.dao.ITravelDao.findById"))
    })
    Orders findByOid(String id) throws Exception;
}


