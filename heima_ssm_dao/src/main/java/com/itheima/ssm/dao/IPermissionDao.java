package com.itheima.ssm.dao;

import com.itheima.ssm.domain.Permission;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface IPermissionDao {
    //权限的dao


    //根据roleid查询权限
    @Select("select*from permission where id in(select PERMISSIONID from role_permission where ROLEID=#{RoleId})")
    List<Permission> findByRoleId(String RoleId);

    @Select("select*from permission")
    List<Permission> findAll() throws Exception;//查询所有权限

    @Insert("insert into permission (PERMISSIONNAME,URL) values(#{permissionName},#{url})")
    void save(Permission permission);//保存权限

    @Select("select*from permission where id not in(select PERMISSIONID from role_permission where ROLEID=#{roleId} )")
    List<Permission> findOtherPermission(String roleId) throws Exception;//查询角色没有的权限

    @Insert("insert into role_permission (PERMISSIONID,ROLEID) values(#{PERMISSIONID},#{roleId})")
    void addPermissionToRole(@Param("roleId") String roleId, @Param("PERMISSIONID") String id) throws Exception;//给角色添加没有的权限
}
