package com.itheima.ssm.dao;

import com.itheima.ssm.domain.UserInfo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IUserDao {
    //用户的登录页面

    @Select("select*from users where username=#{userName}")
    @Results({//多对多查询,查询用户和用户的角色
            @Result(id = true, column = "id", property = "id"),
            @Result(column = "EMAIL", property = "email"),
            @Result(column = "USERNAME", property = "username"),
            @Result(column = "PASSWORD", property = "password"),
            @Result(column = "PHONENUM", property = "phoneNum"),
            @Result(column = "STATUS", property = "status"),
            @Result(column = "id", property = "roles", javaType = java.util.List.class,
                    many = @Many(select = "com.itheima.ssm.dao.IRoleDao.findRolesByUid"))

    })
    UserInfo loginUser(String userName) throws Exception;

    @Select("select*from users")
    List<UserInfo> findAll() throws Exception;


    //对用保存用户
    @Insert("insert into users (EMAIL,USERNAME,PASSWORD,PHONENUM,STATUS) values (#{email},#{username},#{password},#{phoneNum},#{status})")
    void saveUser(UserInfo userInfo) throws Exception;

    //根据用户id查询
    @Select("select*from users where id=#{id}")
    @Results({@Result(id = true, column = "id", property = "id"),
            @Result(column = "EMAIL", property = "email"),
            @Result(column = "USERNAME", property = "username"),
            @Result(column = "PASSWORD", property = "password"),
            @Result(column = "PHONENUM", property = "phoneNum"),
            @Result(column = "STATUS", property = "status"),
            @Result(column = "id", property = "roles", javaType = List.class,
                    many = @Many(select = "com.itheima.ssm.dao.IRoleDao.findByUserId"))
    })
    UserInfo findById(String id) throws Exception;

    @Insert("insert into users_role(userId,roleId) values(#{userId},#{roleId})")
//给用户添加没有的角色
    void addRoleToUser(@Param("userId") String userId, @Param("roleId") String roleId) throws Exception;
    //mybaitis使用注解配置给传递多个参数时需要用@Param注解 相当于params.put("userId",userId) 然后占位符再根据key取value值

    @Update("update users set username=#{username},EMAIL=#{email},PHONENUM=#{phoneNum},STATUS=#{status} where id=#{id}")
    void update(UserInfo userInfo) throws Exception;

    @Delete("delete from users_role where USERID=#{useId}")
    void deleteUserAndRole(String useId) throws Exception;//删除复合主键

    @Delete("delete from users where id=#{useId}")
    void deleteUser(String useId) throws Exception;//删除user记录

    @Delete("delete from users_role where USERID=#{userId} and ROLEID=#{roleId}  ")
    void deleteByUidAndRoleId(@Param("userId") String userId, @Param("roleId") String roleId) throws Exception;//删除用户所持有的角色,即删除关联表记录
}


