package com.itheima.ssm.dao;

import com.itheima.ssm.domain.Member;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface IMemberDao {


    //根据id查询用户
    @Select("select*from member where id=#{mid}")
    @Results({
            @Result(id = true, property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "nickname", column = "NICKNAME"),
            @Result(property = "phoneNum", column = "PHONENUM"),
            @Result(property = "email", column = "EMAIL")
    })
    Member findByMid(String mid) throws Exception;

}
