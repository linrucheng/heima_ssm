package com.itheima.ssm.dao;

import com.itheima.ssm.domain.SysLog;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ISysLogDao {

    @Insert("insert into syslog (VISITTIME,USERNAME,IP,URL,EXECUTIONTIME,METHOD) VALUES(" +
            "#{visitTime},#{username},#{ip},#{url},#{executionTime},#{method})")
    void save(SysLog sysLog);//保存日志

    @Select("select*from syslog")
    List<SysLog> findAll() throws Exception;//查询所有日志
}
