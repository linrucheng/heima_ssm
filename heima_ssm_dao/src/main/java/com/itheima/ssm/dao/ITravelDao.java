package com.itheima.ssm.dao;

import com.itheima.ssm.domain.Traveller;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ITravelDao {

    //查询travel
    @Select("select *from traveller where id in (select ot.TRAVELLERID from order_traveller ot where ot.orderid=#{id})")
    @Results({
            @Result(id = true, column = "id", property = "id"),
            @Result(column = "name", property = "name"),
            @Result(column = "sex", property = "sex"),
            @Result(column = "phonenum", property = "phoneNum"),
            @Result(column = "CREDENTIALSTYPE", property = "credentialsType"),
            @Result(column = "CREDENTIALSNUM", property = "credentialsNum"),
            @Result(column = "TRAVELLERTYPE", property = "travellerType")
    })
    List<Traveller> findById(String id) throws Exception;//order的id

}
