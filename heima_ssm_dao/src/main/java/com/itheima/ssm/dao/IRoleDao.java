package com.itheima.ssm.dao;

import com.itheima.ssm.domain.Role;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface IRoleDao {

    //根据用户的id查询所有角色

    @Select("select*from role where id in(select ROLEID from users_role where USERID =#{id})")
    List<Role> findRolesByUid(String id) throws Exception;


    @Select("select*from role where id in(select ROLEID from users_role where USERID =#{id})")
    @Results({@Result(id = true, column = "id", property = "id"),
            @Result(column = "ROLENAME", property = "roleName"),
            @Result(column = "ROLEDESC", property = "roleDesc"),
            @Result(column = "id", property = "permissions", javaType = List.class,
                    many = @Many(select = "com.itheima.ssm.dao.IPermissionDao.findByRoleId"))
    })
    List<Role> findByUserId(String id);//根据用户id查询所有的角色,在根据角色查询权限 多对多查询

    @Select("select*from role")
    List<Role> findAll() throws Exception;//查询所有角色


    @Insert("insert into role (ROLENAME,ROLEDESC) values(#{roleName},#{roleDesc})")
    void save(Role role) throws Exception;//保存角色

    @Select("select*from role where id not in(select roleid from users_role where userid=#{userId})")
    List<Role> findOtherRole(String userId);
}

