package com.itheima.ssm.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @program: heima_ssm
 * @name: DateUtil
 * @author;lin
 * @create: 2019-06-18 18:00
 **/
public class DateUtil {

    //将日期转化为字符串
    public static String date2String(Date date, String patt) {
        SimpleDateFormat saf = new SimpleDateFormat(patt);
        String dateStr = saf.format(date);
        return dateStr;
    }

}




