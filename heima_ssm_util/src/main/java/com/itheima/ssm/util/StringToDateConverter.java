package com.itheima.ssm.util;

import org.springframework.core.convert.converter.Converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @program: heima_ssm
 * @name: Dateutil
 * @author;lin
 * @create: 2019-06-17 14:47
 * 字符串类型的日期类型转换
 **/
public class StringToDateConverter implements Converter<String, Date> {


    @Override
    public Date convert(String source) {
        if (source == null) {
            throw new RuntimeException("数据异常");
        }
        //2019-06-12 15:25
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try {
            Date date = sdf.parse(source);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
            throw new RuntimeException("转换错误");
        }
    }
}






